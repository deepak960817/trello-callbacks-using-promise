const fs = require('fs');
const board  = require('./data/boards.json');
const list = require('./data/lists.json');
const card = require('./data/cards.json');

function cardInfo(card, id)
{
    return new Promise((resolve, reject) => {
        if(id)
        {
            resolve(() =>{

                const cardData = Object.entries(card).reduce((acc, curr) => {

                    if(curr[0] == id)
                    {
                        acc = curr;
                    }
                    return acc;
                },{})

                return(cardData)
            })
        }
        else
        {
            reject("Can't fetch data, id not correct");
        }
    })
}

const promise3 = cardInfo(card, 'jwkh245');

// promise3
// .then((data) => console.log(data()))
// .catch((err) => console.log(err)); 

module.exports = cardInfo;