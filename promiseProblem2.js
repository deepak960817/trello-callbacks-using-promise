const fs = require('fs');
const board  = require('./data/boards.json');
const list = require('./data/lists.json');
const card = require('./data/cards.json');

function listInfo(list, id)
{
    return new Promise((resolve, reject) => {
        if(id)
        {
            resolve(() =>{

                const listData = Object.entries(list).reduce((acc, curr) => {

                    if(curr[0] == id)
                    {
                        acc = curr[1];
                    }
                    return acc;
                },{})

                return(listData)
            })
        }
        else
        {
            reject("Can't fetch data, id not correct");
        }
    })
}

const promise2 = listInfo(list, 'mcu453ed');

// promise2
// .then((data) => console.log(data()))
// .catch((err) => console.log(err));

module.exports = listInfo;