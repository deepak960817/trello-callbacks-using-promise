const fs = require('fs');
const board  = require('./data/boards.json');
const list = require('./data/lists.json');
const card = require('./data/cards.json');

function boardInfo(board, id)
{
    return new Promise((resolve, reject) => {
        if(id)
        {
            resolve(() =>{

                const boardData = board.reduce((acc, curr) => {

                    if(curr["id"] == id)
                    {
                        acc = curr;
                    }
                    return acc;
                },{})

                return(boardData)
            })
        }
        else
        {
            reject("Can't fetch data, id not correct");
        }
    })
}

const promise1 = boardInfo(board, 'mcu453ed');

// promise1
// .then((data) => console.log(data()))
// .catch((err) => console.log(err));

module.exports = boardInfo;

