const fs = require('fs');
const board  = require('./data/boards.json');
const list = require('./data/lists.json');
const card = require('./data/cards.json');

const problem1 = require('./promiseProblem1');
const problem2 = require('./promiseProblem2');
const problem3 = require('./promiseProblem3');

function getInformation(name)
{
    return new Promise((resolve, reject) => {
        if(name)
        {
            resolve(() => {
                let boardID = board.reduce((accumulator, current) => {
        
                    if(current["name"] === name)
                    {
                        accumulator = current["id"];
                    }
                    return accumulator;
                },"")

                return boardID;
            })
        }
        else
        {
            reject("Cant fetch ID");
        }
    })
}    


const promise = getInformation("Thanos");

promise
.then((data) => {
    const  id = data();
    return problem1(board, id);
})
.then((content) => {
    const boardelement = content();
    console.log(boardelement);
    return problem2(list, boardelement["id"]);
})
.then((content) => {
    const listelement = content();
    console.log(listelement);
    const listID = listelement.reduce((acc, curr) => {
        acc.push(curr['id']);
        return acc;
    },[]);
    
    let p = [];
    for(let index=0; index<listID.length; index++)
    {
        p[index] = problem3(card, listID[index])
    }
    return Promise.all(p);
})
.then((cardData) => {
    for(let index=0; index<cardData.length; index++)
    {
        console.log(cardData[index]());
    }
})
.catch((err) => console.log(err));



